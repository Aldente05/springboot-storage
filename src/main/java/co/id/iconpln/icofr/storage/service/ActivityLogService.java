package co.id.iconpln.icofr.storage.service;

import co.id.iconpln.icofr.storage.AppStorage;
import co.id.iconpln.icofr.storage.entity.ActivityLog;
import co.id.iconpln.icofr.storage.repository.ActivityLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by f.putra on 30/01/18.
 */
@Service
public class ActivityLogService {

    @Autowired
    ActivityLogRepository activityLogRepository;
    @Autowired
    MongoTemplate mongoTemplate;

    /**
     * save activity user
     *
     * @param log
     * @return
     */
    public ActivityLog save(ActivityLog log) {
        try {
            return activityLogRepository.insert(log);
        } catch (Exception ex) {
            AppStorage.getLogger(this).error(ex.getMessage());
            return null;
        }
    }

    /**
     * get activity user by periode
     *
     * @param userId
     * @param pageable
     * @return
     */
    @Transactional(readOnly = true)
    public List<ActivityLog> loadByIntervalFilterByUserId(Long userId, Pageable pageable) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("userId").is(userId));
            query.with(pageable);

            return mongoTemplate.find(query,ActivityLog.class);

        } catch (Exception ex) {
            AppStorage.getLogger(this).error(ex.getMessage());
            return null;
        }
    }


}
