package co.id.iconpln.icofr.storage;

import co.id.iconpln.icofr.storage.config.StorageConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * Created by f.putra on 03/02/18.
 */

@SpringBootApplication
@Import(StorageConfig.class)
public class AppStorage {

    private static class AppCoreHolder {

        private static final AppStorage INSTANCE = new AppStorage();
    }

    public static AppStorage getInstance() {
        return AppCoreHolder.INSTANCE;
    }

    public static Logger getLogger(Object o) {
        return LoggerFactory.getLogger(o.getClass());
    }
}
