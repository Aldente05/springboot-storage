package co.id.iconpln.icofr.storage.exception;

/**
 * Created by f.putra on 25/01/18.
 */
public class StorageException extends RuntimeException {

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
