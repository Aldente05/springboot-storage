package co.id.iconpln.icofr.storage.repository;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by f.putra on 25/01/18.
 */
public interface StorageRepository {

    void init();

    Map<String, Object> store(MultipartFile file);

    List<Map<String, Object>> listStore(List<MultipartFile> file);

    void store(String path, InputStream iStream);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

}

