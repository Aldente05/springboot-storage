package co.id.iconpln.icofr.storage.repository;

import co.id.iconpln.icofr.storage.entity.ActivityLog;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by f.putra on 30/01/18.
 */
public interface ActivityLogRepository extends MongoRepository<ActivityLog, Long> {
}
