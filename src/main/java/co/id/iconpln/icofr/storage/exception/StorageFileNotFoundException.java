package co.id.iconpln.icofr.storage.exception;

/**
 * Created by f.putra on 25/01/18.
 */
public class StorageFileNotFoundException extends StorageException {

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
