package co.id.iconpln.icofr.storage.config;

import co.id.iconpln.icofr.storage.common.StorageProperties;
import co.id.iconpln.icofr.storage.repository.ActivityLogRepository;
import co.id.iconpln.icofr.storage.repository.StorageRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by f.putra on 03/02/18.
 */
@EnableMongoRepositories(basePackageClasses = ActivityLogRepository.class)
@EnableConfigurationProperties(StorageProperties.class)
@ComponentScan(basePackages = {"co.id.iconpln.icofr.storage.service", "co.id.iconpln.icofr.storage.common"})
@EntityScan("co.id.iconpln.icofr.storage.entity")
@EnableTransactionManagement
public class StorageConfig {

    @Bean
    CommandLineRunner init(StorageRepository storageRepository) {
        return (args) -> {
            storageRepository.init();
        };
    }
}
