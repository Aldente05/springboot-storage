package co.id.iconpln.icofr.storage.common;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by f.putra on 25/01/18.
 */
@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private String location = "upload-dir";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
