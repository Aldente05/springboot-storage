package co.id.iconpln.icofr.storage.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by f.putra on 30/01/18.
 */
@Getter
@Setter
@Document(collection = "activityLog")
public class ActivityLog {

    @Id
    private BigInteger id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "session")
    private String session;

    @Column(name = "login_time")
    private Date login_at;

    @Column(name = "logout")
    private Date logout_at;
}
