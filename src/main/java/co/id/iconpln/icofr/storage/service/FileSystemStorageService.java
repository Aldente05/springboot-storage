package co.id.iconpln.icofr.storage.service;

import co.id.iconpln.icofr.storage.common.StorageProperties;
import co.id.iconpln.icofr.storage.exception.StorageException;
import co.id.iconpln.icofr.storage.exception.StorageFileNotFoundException;
import co.id.iconpln.icofr.storage.repository.StorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by f.putra on 25/01/18.
 */
@Service
public class FileSystemStorageService implements StorageRepository {

    private final Path rootLocation;

    /**
     * get root path from system
     * @param properties
     */
    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    /**
     * store multipart file to root location
     * @param file
     */
    @Override
    public Map<String, Object> store(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current adirectory "
                                + filename);
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(filename),
                    StandardCopyOption.REPLACE_EXISTING);

            Map<String, Object> result = new HashMap<>();
            result.put("path", this.rootLocation);
            result.put("filename", filename);

            return result;
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    @Override
    public List<Map<String, Object>> listStore(List<MultipartFile> file) {

        try {
            List<Map<String, Object>> response = new ArrayList<>();
            for(MultipartFile list: file){
                String filename = StringUtils.cleanPath(list.getOriginalFilename());
                if (file.isEmpty()) {
                    throw new StorageException("Failed to store empty file " + filename);
                }
                if (filename.contains("..")) {
                    // This is a security check
                    throw new StorageException(
                            "Cannot store file with relative path outside current adirectory "
                                    + filename);
                }
                Files.copy(list.getInputStream(), this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);

                Map<String, Object> result = new HashMap<>();
                result.put("path", this.rootLocation);
                result.put("filename", filename);

                response.add(result);
            }
            return response;
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file ", e);
        }
    }


    /**
     * save file on path and with stream
     * @param path
     * @param iStream
     */
    @Override
    public void store(String path,InputStream iStream) {
      String cleanPath = StringUtils.cleanPath(path);

      try{
        if(!Files.exists(rootLocation)) {
          Files.createDirectories(rootLocation);
        }
        Path parentDir = this.rootLocation.resolve(cleanPath).getParent();
        if(!Files.exists(parentDir)) {
          Files.createDirectories(parentDir);
        }
        Files.copy(iStream, this.rootLocation.resolve(cleanPath),StandardCopyOption.REPLACE_EXISTING);
      }catch(IOException e){
        throw new StorageException("Failed to store file " + path, e);
      }
    }

    /**
     * load all file from root Directory
     * @return
     */
    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(path -> this.rootLocation.relativize(path));
        }
        catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    /**
     * load one file by filename
     * @param filename
     * @return
     */
    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    /**
     * load resource file by file name
     * @param filename
     * @return
     */
    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);

            }
        }
        catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    /**
     * delete all file on root Directory with recrusive function clean
     */
    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    /**
     * initiate root location directory for storage service
     */
    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }
}